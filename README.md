Hasty WordPress
===============

Hasty WordPress is a super-fast way to get a modern-looking, super-fast WordPress instance up and running quickly. It includes some helpful plugins and uses [Roots Theme](http://www.rootstheme.com/) (Based on [HTML5 Boilerplate](http://html5boilerplate.com/) & [Twitter Bootstrap](http://twitter.github.io/bootstrap/)) to make everything mobile-friendly and pretty.

I created Hasty WordPress because I wanted an easy way to bootstrap my WordPress sites without needing to rebuild a standard set of plugins each time.

Combine this with [XAMPP](http://www.apachefriends.org/en/xampp.html) for your dev environment and you've got yourself a stew goin'.

## WordPress

* [WordPress](http://http://wordpress.org/) - 3.6

## Themes

* [Roots Theme](http://www.rootstheme.com/) - g7c13142

## Plugins

* [Advanced Access Manager](http://wordpress.org/plugins/advanced-access-manager/) - 1.7.5
* [Akismet](http://wordpress.org/plugins/akismet/) - 2.5.9
* [Contact Form 7](http://wordpress.org/plugins/contact-form-7/) - 3.6
* [Display Posts Shortcode](http://wordpress.org/plugins/display-posts-shortcode/) - 2.3
* [Disqus Comment System](http://wordpress.org/plugins/disqus-comment-system/) - 2.74
* [Google Analytics for WordPress](http://wordpress.org/plugins/google-analytics-for-wordpress/) - 4.3.3
* [Really Simple Captcha](http://wordpress.org/plugins/really-simple-captcha/) - 1.6
* [Responsive Slider](http://wordpress.org/plugins/responsive-slider/) - 0.1.8
* [WP Google Fonts](http://wordpress.org/plugins/wp-google-fonts/) - 3.11
* [WP Super Cache](http://wordpress.org/plugins/wp-super-cache/) - 1.3.2

## Add-Ons

* [SocialSharePrivacy](https://github.com/panzi/SocialSharePrivacy) - 65176db2bdf684e3d16182dd2be1a29a8b7af685
    * By default, Google Plus, Twitter, Facebook, and Email sharing are enabled, but may be customized in `wp-content/themes/roots/templates/head.php` (See comments in this page)
* [humans.txt](http://humanstxt.org/) - This file is used for crediting site authors and technology used. You should edit this and give credit to the authors of your site.

# Contributing

I love pull requests. If you see an out-of-date plugin or something I should include (or remove), send a pull request, lets talk.
