<footer class="content-info" role="contentinfo">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> - <a href="/humans.txt"><img src="/media/humanstxt-isolated-blank.gif"></a> <a href="http://humanstxt.org/" target="_blank">About humans.txt</a></p>
  </div>
</footer>
<script type="text/javascript">(function () {var s = document.createElement('script');var t = document.getElementsByTagName('script')[0];s.type = 'text/javascript';s.async = true;s.src = '/wp-includes/SocialSharePrivacy/jquery.socialshareprivacy.min.autoload.js';t.parentNode.insertBefore(s, t);})();</script>

<?php wp_footer(); ?>
